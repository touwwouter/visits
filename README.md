# Visits

A group of persons would like to visit something. Several dates have been
suggested and everybody's availability is known. The group is too large to
visit as a single group. We'd like to split the group instead, taking into
account availability and maximum group size. Also, we'd like to keep the
number of visits as low as possible.

Indeed, this problem can be formulated as an Integer Linear Program (ILP). The
mathematical problem statement is shown below (you might want to generate the
more readable pdf version using `make`, see instructions below).

The ILP has been implemented in GNU MathProg. The Usage sections explains how
to solve it.

## The ILP

Given $P \in \{0,1\}^{m \times n}$ indicating the availability of person $i \hspace{.1cm} (i=1..m)$
at date $j \hspace{.1cm} (j=1..n)$ and fixed visit capacity $c$, assign every person exactly
one visit while minimizing the number of total visits.

\begin{equation}
\begin{array}{rlclclcl}
\displaystyle \min & \multicolumn{4}{l}{\displaystyle \sum_j u_j} \\
\textrm{s.t.} & \displaystyle \sum_i P_{i,j}D_{i,j} & \leq & cu_j & \forall j \in n\\
&\displaystyle \sum_j P_{i,j}D_{i,j} & = & 1 & \forall i \in m\\
& u \in \{0,1\}^n & & &\\
& D \in \{0,1\}^{m \times n} & & &\\
\end{array}
\end{equation}

## Usage

### Prerequisites

* For the ILP: `glpk` with `glpsol` or any other ILP solver that interpretes GNU MathProg syntax
* `pandoc` with pdflatex for creating the documentation
* GNU `Make` for automating creating and cleaning all output

### Instructions

A simple Makefile has been provided to run the solver and convert markdown files to pdf.

To make all Makefile targets, issue

    make

All output files can be removed as follows:

    make clean
