firstTarget: all

MD = $(wildcard *.md)
MD := $(MD:%.md=%)
PDF = $(MD:%=%.pdf)

.PHONY: firsttarget all clean solve

all: solve $(PDF)

solve: visits.out
	
visits.out: visits.mod visits.dat
	@echo starting solver..
	glpsol -m $< -d $(word 2,$^) > $@

%.pdf: %.md
	@echo creating $@..
	pandoc -o $@ < $<

clean:
	rm -f $(PDF)
	rm -f visits.out
