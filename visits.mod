/* Group visits
 *
 * Given m persons, n visit dates and
 * a binary mxn matrix indicating the availability
 * of a person at a given date,
 * find visit dates for all persons under some constraints. 
 *
 * Wouter Touw, wouter.touw@dsm.com, 2019
 *
 */

param m, integer, > 0;
/* number of persons */

param n, integer, > 0;
/* number of visit dates */

set I := 1..m;
/* set of persons */

set J := 1..n;
/* set of visit dates */

param c, integer, > 0;
/* visit capacity */

param p{i in I, j in J}, binary, default 0;
/* p[i,j] = 1 means person i is available at date j */

var d{i in I, j in J}, binary;
/* d[i,j] = 1 means person i visits at date j */

var used{j in J}, binary;
/* used[j] = 1 means at least 1 person visits at date j */

s.t. one{i in I}: sum{j in J} p[i,j]*d[i,j] = 1; 
/* exactly one visit per person */

s.t. limvisits{j in J}: sum{i in I} p[i,j] * d[i,j] <= c*used[j];
/* at most two unique visits */

minimize obj: sum{j in J} used[j];
/* objective is to minimize the number of visits */

solve;

printf("Person-date availability:\n");
for {i in I}
{
    for {j in J}
    {
        printf " %d", p[i,j];
    }
    printf(" \n");
}
printf(" \n");


printf("Person-date schedule:\n");
for {i in I}
{
    for {j in J}
    {
        printf " %d", d[i,j];
    }
    printf(" \n");
}
printf(" \n");

printf("Selected visits:\n");
for {j in J}
{
    printf " %d", used[j];
}
printf(" \n");
data; 

param m := 4;

param n := 5;

param c:= 2;

param p : 1 2 3 4 5 :=
      1   1 1 . . .
      2   . 1 1 1 1
      3   1 . 1 . .
      4   1 . . . 1 ;

end;
